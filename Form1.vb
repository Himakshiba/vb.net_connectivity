﻿'create database in MS Access
'on path E:\ConnectivityExample\ConnectivityExample\bin\x86\Debug\Database\MyDatabase.mdb
'Save Type Microsoft Access Database (2002-2003 format)
'create table MyTable with following fields
'RNO-number (long Integer)
'RNAME-text
'RADDRESS-text
'RCITY-text
'RCONTACT-number(double)




'if database file is already open in RAM close the Database file and then run the program
Imports System.Data.OleDb

Public Class Form1

    'to establish connecton
    Dim cn As OleDbConnection

    'to insert update delete data
    Dim cmd As OleDbCommand

    'to select data
    Dim adpt As OleDbDataAdapter

    'to load all selected data in dataset
    Dim ds As Data.DataSet

    'for row no.
    Dim i As Integer

    'to count rows
    Dim count As Integer
    Public Sub clearcontrol()
        Me.txtAddress.Clear()
        Me.txtCity.Clear()
        Me.txtContact.Clear()
        Me.txtName.Clear()
        Me.txtRno.Clear()
    End Sub
    Public Sub connectivity()
        'cn = New OleDbConnection("Provider=Microsoft.Jet.OLEDB.4.0;Data Source=E:\ConnectivityExample\ConnectivityExample\bin\x86\Debug\Database\MyDatabase.mdb")
        Dim pro, datasrc As String

        'dynamic path variables
        pro = "Provider=Microsoft.Jet.OLEDB.4.0;"
        datasrc = "Data Source=" & Application.StartupPath & "\Database\MyDatabase.mdb"
        'dynamic path
        cn = New OleDbConnection(pro & datasrc)
        Try
            If cn.State <> ConnectionState.Open Then cn.Open()
        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try
    End Sub


    Public Sub dataload()
        connectivity()
        adpt = New OleDbDataAdapter("SELECT * FROM MYTABLE", cn)
        ds = New Data.DataSet
        adpt.Fill(ds, "My")

        DataGridView1.DataSource = ds
        DataGridView1.DataMember = "My"
        If ds.Tables(0).Rows.Count > 0 Then
            count = ds.Tables(0).Rows.Count
            Me.txtRno.Text = ds.Tables(0).Rows(i).Item(0).ToString
            Me.txtName.Text = ds.Tables(0).Rows(i).Item(1).ToString
            Me.txtAddress.Text = ds.Tables(0).Rows(i).Item(2).ToString
            Me.txtCity.Text = ds.Tables(0).Rows(i).Item(3).ToString
            Me.txtContact.Text = ds.Tables(0).Rows(i).Item(4).ToString
        End If

    End Sub
    Private Sub Form1_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        dataload()
        i = 0
    End Sub

    Private Sub btnInsert_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnInsert.Click
        If btnInsert.Text = "Save" Then
            connectivity()
            Dim str As String
            str = "INSERT INTO MyTable Values(" & Me.txtRno.Text & ",'" & Me.txtName.Text & "','" & Me.txtAddress.Text & "','" & Me.txtCity.Text & "'," & Me.txtContact.Text & ")"
            cmd = New OleDbCommand
            cmd.Connection = cn
            cmd.CommandText = str
            cmd.ExecuteNonQuery()
            MsgBox("Data Inserted")
            btnInsert.Text = "Insert"
            dataload()
        Else
            clearcontrol()
            btnInsert.Text = "Save"
        End If

    End Sub

    Private Sub btnUpdate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnUpdate.Click
        If btnUpdate.Text = "Update" Then
            btnUpdate.Text = "Save"
            txtRno.Enabled = False
        Else
            btnUpdate.Text = "Update"
            connectivity()
            Dim str As String
            str = "UPDATE MYTABLE SET RNAME='" & Me.txtName.Text & "', RADDRESS='" & Me.txtAddress.Text & "', RCITY='" & Me.txtCity.Text & "', RCONTACT=" & Me.txtContact.Text & " WHERE RNO=" & Me.txtRno.Text
            cmd = New OleDbCommand
            cmd.Connection = cn
            cmd.CommandText = str
            cmd.ExecuteNonQuery()
            MsgBox("Data Updated")
            txtRno.Enabled = True
            dataload()
        End If
    End Sub

    Private Sub btnDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        connectivity()
        Dim str As String
        str = "DELETE FROM MYTABLE WHERE RNO=" & Me.txtRno.Text
        cmd = New OleDbCommand
        cmd.Connection = cn
        cmd.CommandText = str
        cmd.ExecuteScalar()
        MsgBox("Data Deleted")
        dataload()
    End Sub

    Private Sub btnFirst_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnFirst.Click
        If count = 0 Then
            MsgBox("No Data Found")
        Else
            If i = 0 Then
                MsgBox("Already on First Record")
            Else
                i = 0
                dataload()
            End If
        End If


    End Sub

    Private Sub btnPre_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPre.Click
        If count = 0 Then
            MsgBox("No Data Found")
        Else
            If i = 0 Then
                MsgBox("Already on First Record")
            Else
                i = i - 1
                dataload()
            End If
        End If

    End Sub

    Private Sub btnNext_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNext.Click
        If count = 0 Then
            MsgBox("No Data Found")
        Else
            If i = count - 1 Then
                MsgBox("Already on Last Record")
            Else
                i = i + 1
                dataload()
            End If
        End If

    End Sub

    Private Sub btnLast_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnLast.Click
        If count = 0 Then
            MsgBox("No Data Found")
        Else
            If i = count - 1 Then
                MsgBox("Already on Last Record")
            Else
                i = count - 1
                dataload()
            End If
        End If

    End Sub
End Class
